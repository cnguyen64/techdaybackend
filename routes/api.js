var	Beacon = require('../models/beacon').Beacon,
	User = require('../models/user').User,
	Properties = require('../models/properties').Properties,
	Winner = require('../models/winner').Winner;
	Flag = require('../models/flag').Flag; 

module.exports = function(app){
	//get list beacon.
	app.get('/api/listbeacon', function(req, res){
		Beacon.find(function(err, returnBeacons){
			if(err){
				res.json(500, { success : false, message: err });
			}else if(returnBeacons && returnBeacons.length > 0){
				// res.json(200, {success: true, listbeacon:returnBeacons});
				res.render('index.html', {
					listbeacon: returnBeacons					
				});		
			}
		});
		
	});
	//end get list beacon.

	//Get Flag view
	app.get('/api/response', function(req, res){
		
		Flag.find({flagID: '1'},function(err, returnFlag){

			if(err)
			{
				res.json(500, { success : false, message: err });
			}
			else if(returnFlag){
				console.log(returnFlag[0].isOn);
				res.render('response.html',{
					flag: returnFlag[0]
				});
			}
		});
		
	});
	//End get Flag view

	//Turn on flag
	app.post('/api/turnon', function(req, res){
			Flag.update({flagID : '1'}, {'$set':{isOn : true}}, function(err,result)
				{

					if(!err){
									console.log('Turn on succes');
									 res.redirect('/api/response');
					}else
					{
							res.json(500, {success : false, message : err});
						
					}	
				});
	});
	//End turn on flag


	//Turn off flag
	app.post('/api/turnoff', function(req, res){
			Flag.update({flagID : '1'}, {'$set':{isOn : false}}, function(err,result)
				{

					if(!err){
									console.log('Turn off succes');
									res.redirect('/api/response');
					}else
					{

							res.json(500, {success : false, message : err});
						
					}	
				});
	});
	//End turn off flag
			

	//get flag
	app.get('/api/getflag',function(req,res){

		Flag.find({flagID: '1'},function(err, returnFlag){
			if(err)
			{
				res.json(500, { success : false, message: err });
			}
			else if(returnFlag){
				console.log(returnFlag[0].isOn);
				res.json(200, {success: true, isOn : returnFlag[0].isOn});

			}
		});
	});
	//End get flag

	//get each beacon for editting.
	app.get('/api/edit/:idnumber', function(req, res){
		var idnumber = req.params.idnumber;
		Beacon.find({idnumber : idnumber} ,function(err, returnBeacons){
			if(err){
				res.json(500, { success : false, message: err });
			}else if(returnBeacons && returnBeacons.length > 0){
				// res.json(200, {success: true, listbeacon:returnBeacons});
				res.render('edit_beacon.html', {
					// listbeacon: returnBeacons
					beacon : returnBeacons[0]					
				});		
			}
		});
		
	});
	//end get each beacon for editting.




	// post user info
	app.post('/api/userinfo', function(req, res){
		console.log('call api /api/userinfo');
		var userId = req.body.userId,
			username = req.body.username,
			email = req.body.email;
		//query USER.
		User.find({userId : userId}, function(err, returnUser){
			if(err){
				res.json(500, { success : false, message: err });
			}
			//user was exist.
			if(returnUser){
				if (returnUser.length > 0){
					var userObject = returnUser[0];
					//update dependency
					Beacon.find(function(err, returnBeacons){
						if(err){
							res.json(500, {success : false, message: err });
						}else if(returnBeacons && returnBeacons.length > 0){
							for (var i = 0; i < returnBeacons.length; i++) {
								var beaconsObject = returnBeacons[i];
								User.update({userId : userId, 'beacon.idnumber': i+1 },
								{'$set':{'beacon.$.dependency' : beaconsObject.dependency}},function(err, result){
									if(err){
										console.log("error : " + err);
									}	
								});
							};
						}
					});
					//update properties
					Properties.find(function(err, returnProperties){
						if(err){
							res.json(500, {success : false, message: err });
						}else if(returnProperties && returnProperties.length > 0){
							var propertiesObject = returnProperties[0];
							// console.log("insert properties");
							// userObjectInserted.properties.push(propertiesObject);	
							User.update({userId : userId, 'properties.repeattime': 3 },
							{'$set':{'properties.$.distance' : propertiesObject.distance}},function(err, result){
								if(err){
									res.json(500, {success : false, message: err });
								}else{
									User.find({userId : userId}, function(err, returnUser){
										if(err){
											res.json(500, { success : false, message: err });
										}
										if(returnUser){
											if (returnUser.length > 0){
												res.json(200, {success: true, userInfo : returnUser[0]});	
											}
										}
									});	
								}

							});
						}
					});
			

					// res.json(200, {success: true, userInfo : userObject})	
				}else{ //not yet have user --> create user
					var newUser = new User({
						userId : userId,
						username : username,
						email : email,
						beaconFound : 0,
					});
					//save user
					newUser.save(function(err) {
						if(!err) {
							User.find({userId : userId}, function(err, returnUser){
								if(err){
									res.json(500, { success : false, message: err });
								}

								if(returnUser && returnUser.length > 0){
									var userObjectInserted = returnUser[0];
									//create properties for user
									Properties.find(function(err, returnProperties){
										if(err){
											res.json(500, {success : false, message: err });
										}else if(returnProperties && returnProperties.length > 0){
											var propertiesObject = returnProperties[0];
											// console.log("insert properties");
											userObjectInserted.properties.push(propertiesObject);	
										}
									});
									//create beacon for user	
									Beacon.find(function(err, returnBeacons){
										if(err){
											res.json(500, {success : false, message: err });
										}else if(returnBeacons && returnBeacons.length > 0){
											var beaconObject;
											for(var i = 0; i < returnBeacons.length; i++){
												beaconObject = returnBeacons[i];
												userObjectInserted.beacon.push(beaconObject);	
											}
											//save USER INFO with BEACON.
											userObjectInserted.save(function(err) {
												if(!err) {
													//return USER INFO WITH BEACON
													User.find({userId : userId}, function(err, returnUser){
														if(err){
															res.json(500, {success : false, message: err });
														}
														if(returnUser){
															if (returnUser.length > 0){
																var userObject = returnUser[0];
																res.json(200, {success: true, userInfo : userObject});	
															}  
														}
													});		 
												} else {
												    res.json(500, {success : false, message: "User with beacon array insert fail. Error : " + err});
												}  
											});		
										}
									}).sort({idnumber : 1});
								}
							});  
						} else {
						    res.json(500, {success: false, message: "Insert User fail. Error : " + err});
						}  
					});	
					
				}
			}

		});

	});
	//end post user infor
	//update edit beacon info
	app.post('/api/editbeacon', function(req, res){
		var idnumber = req.body.idnumber,
			major = req.body.major,
			minor = req.body.minor,
			name = req.body.name,
			clue = req.body.clue,
			dependency = req.body.dependency;
		if(!idnumber || !minor || !major || !name || !clue){
			// res.render('edit_beacon.html', {error:"Id number, major, minor, name, clue could not be empty"});	
			res.send(500,'showAlert') 
		}else{
			Beacon.find({idnumber : idnumber}, function(err, returnBeacons){

				if(err){
					res.json(500,{success : false, message : err});
				}
				if(returnBeacons && returnBeacons.length > 0){
					console.log("find out beaon" + returnBeacons[0].idnumber);
					Beacon.update({idnumber : idnumber},{major : major, minor : minor, name : name, dependency : dependency, clue : clue},
					function(err, result){
						if(err){
							res.json(500,{success : false, message : err});
						}

						// res.json(200,{success : true, message :"return beacon" + returnBeacons[0]});
						res.end('{"success" : "Updated Successfully", "status" : 200}');
						// res.redirect('/api/listbeacon');
					})
				}
			});
		}	
	});
	//end update edit beacon info
	
	//update beacon detected = true
	app.post('/api/updatebeacon', function(req, res){
		var userId = req.body.userId,
			date = new Date().toString(),
			idnumber = req.body.idnumber;
				User.find({userId : userId, beacon :{'$elemMatch':{detected: false, idnumber: idnumber}}}, function(err, returnUser){
					if(err){
						res.json(500, {success : false, message : err });
					}
					if(returnUser && returnUser.length > 0){
						var beaconObject = returnUser[0];
						User.update({userId : userId}, {'$set':{'beaconFound' : beaconObject.beaconFound + 1}}, function(err, result){
							if(!err){
								User.update({userId : userId, 'beacon.idnumber' : idnumber},
								{'$set':{'beacon.$.detected' : true, 'beacon.$.date': date}, '$push':{'beacon.$.detectingcount' : date}}, 
								function(err, result){
									if(!err){
										var now = new Date(),
											currentHour= now.getHours();

										if(currentHour > 30 ){
											res.json(200, {success : true, caution: true, message:  userId + " Update beacon " + idnumber +"to true success on: " + date +" / " + currentHour});
										}else{
											res.json(200, {success : true, caution: false, message:  userId + " Update beacon " + idnumber +"to true success on: " + date+" / " + currentHour});	
										}
										
									}else{
										res.json(500, {success : false, message : err});
									}	
								});
							}else{
								console.log(err);
							}
						});
						
					}else{
						res.json(200, {success : false, message : "This beacon already true."});

					}
				});	
	});
	//end update beacon detected = true

	//update beacon detecting count
	app.post('/api/updatedetectingcount', function(req, res){
		var userId = req.body.userId,
			date = new Date(),
			idnumber = req.body.idnumber;

		User.find({userId : userId, beacon :{'$elemMatch':{idnumber: idnumber}}}, function(err, returnUser){
			if(err){
				res.json(500, {success : false, message : err });
			}
			if(returnUser && returnUser.length > 0){
				var beaconObject = returnUser[0],
					time = beaconObject.beacon[idnumber-1].detectingcount,//get array time update.
					lastupdate = time[time.length - 1],//lasttime update detect beacon.
					now = new Date(),
					last = new Date(lastupdate);

				if(last && ((now - last)/60000 > 3)){
					User.update({userId : userId, 'beacon.idnumber' : idnumber},
					{'$push':{'beacon.$.detectingcount' : date}}, 
					function(err, result){
						if(!err){
							res.json(200, {success : true, message: userId + " Update beacon " + idnumber +" detectingcount on: " + date + " Time: " + last +"/"+(now - last)/60000});
						}else{
							res.json(500, {success : false, message : err});
						}	
					});
				}else{ 
					res.json(200, {success : false, message : "Not enough time for updating" + last});	
				}
			}else{
				res.json(200, {success : false, message : "Did not find beacon " + idnumber});

			}
		});

	});
	//end update beacon detecting count
	app.post('/api/editdatabase', function(req, res){
		User.find({beacon :{'$elemMatch':{detected: true }}}, function(err, returnUser){
			if(err){
				res.json(500, {success : false, message : err });
			}
			if(returnUser && returnUser.length > 0){
				for (var i = 0; i < returnUser.length; i++) {
					for (var j = 0; j < returnUser[i].beacon.length; j++) {
						if(returnUser[i].beacon[j].detected){
							var date = new Date(returnUser[i].beacon[j].detectingcount[0]),
							olddate = returnUser[i].beacon[j].detectingcount[0],
							username = returnUser[i].username,
							idnumber = returnUser[i].beacon[j].idnumber;
							// console.log(idnumber);
							// console.log(date);
							User.update({username : username, 'beacon.idnumber' : idnumber},
								{'$set':{"beacon.$.detectingcount.0" : date}}, 
								function(err, result){
									if(!err){
											// res.json(200, {success : true, message: date});
											 // console.log(result);
										}else{
											res.json(200, {success : false, message : err});	
											console.log(false);
										}	
							});
						}
						
					}
				}
			}
		});
	});

	//update beacon detected = false
	app.post('/api/updatebeaconfalse', function(req, res){
		var username = req.body.username,
			idnumber = req.body.idnumber;
		User.find({username : username, beacon :{'$elemMatch':{detected: true, idnumber: idnumber}}}, function(err, returnUser){
			if(err){
				res.json(500, {success : false, message : err });
			}
			if(returnUser && returnUser.length > 0){
				User.update({username : username, 'beacon.idnumber' : idnumber},
				{'$set':{'beacon.$.detected' : false}}, 
				function(err, result){
					if(!err){
						res.json(200, {success : true, message: username + " Update to false" + idnumber +" success."});
					}else{
						res.json(500, {success : false, message : err});
					}	
				});
			}else{
				res.json(200, {success : false, message : "This beacon already false."});

			}
		});

	});
	//end update beacon detected = false

	//save winner
	app.post('/api/savewinner', function(req, res){
		var username = req.body.username,
			userId = req.body.userId,
			date = new Date();
		Winner.find({userId : userId}, function(err, returnUser){
			if(err){
				res.json(500, {success : false, message : err });
			}
			if(returnUser && returnUser.length > 0){
				res.json(200, {success : false, message: "User is exist"});			
			}else{
				Winner.collection.insert({userId : userId, username : username, date: date},{}, 
					function(err, returnedWinner){
					if(err) {
						res.json(500, {message: "Could not insert new winner. Error: " + err});
					}
						//CREATED 201
					res.status(201).json({success : true, message : "Winner inserted"});	
				});
			}

			

		});

	});
	//end save winner

	//get clue of beacon
	app.get('/api/clue/:idnumber',function(req,res){
		var idnumber = parseInt(req.params.idnumber);
		console.log("idnumber :" + idnumber);
		Clue.find({idnumber : idnumber}, function(err, returnClue){
			if(err){
				res.json(500, {success : false, message : err });
			}
			if(returnClue && returnClue.length > 0){
				var clueObject = returnClue[0];
				res.json(200, {success : true, clue: clueObject.clue});
			}else{
				res.json(200, {success : false, message: "Do not have clue for this beacon."});
			}
		});
	});
	//end get clue of beacon

//resetdata
	app.post('/api/resetdata', function(req, res){

		var username = req.body.username;
		User.find({username : username}, function(err, returnUser){
			if(err){
				res.json(500, {success : false, message : err });
			}
			if(returnUser && returnUser.length > 0){
				var userremove = returnUser[0];
				User.remove({username: userremove.username}, function(err, removed){
					if(err){
						// res.json(500, {message: "Could not reset data. Error: " + err});
						
						res.render('reset_data.html', {error:"Could not reset data. Error: " + err});	
						
						

					}
					
					res.render('reset_data.html', {error: "Reset data of "+ userremove.username + " is successful." });
						
					// res.status(201).json({success : true, message : "Reset data of "+ userremove.username + "is successful" });	
				});	
			}else{
			
				res.render('reset_data.html', {error:  "Sorry! User " + username + " does not exist in our database." });
				
				// res.status(201).json({success : false, message : userremove.username + " Nonexist"});	
			}
		});

	});
//end resetdata
//Delete winer
	app.post('/api/deletewinner', function(req, res){

		var username = req.body.username;
		Winner.find({username : username}, function(err, returnUser){
			if(err){
				res.json(500, {success : false, message : err });
			}
			if(returnUser && returnUser.length > 0){
				var userremove = returnUser[0];
				Winner.remove({username: userremove.username}, function(err, removed){
					if(err){
						// res.json(500, {message: "Could not reset data. Error: " + err});
						
						res.render('reset_data.html', {error1:"Could not reset data. Error: " + err});	
						
						

					}
					
					res.render('reset_data.html', {error1: "Reset data of winner "+ userremove.username + " is successful." });
						
					// res.status(201).json({success : true, message : "Reset data of "+ userremove.username + "is successful" });	
				});	
			}else{
			
				res.render('reset_data.html', {error1:  "Sorry! User " + username + " does not exist in our database." });
				
				// res.status(201).json({success : false, message : userremove.username + " Nonexist"});	
			}
		});

	});
//end Delete winer
	//get list of winners.
	app.get('/api/winners', function(req, res){
		Winner.find(function(err, returnWinners){
			if(err){
				res.json(500, { success : false, message: err });
			}else if(returnWinners){
				console.log(JSON.stringify(returnWinners));
				res.render('winners.html', {
					winners: returnWinners,
					displayTime: new Date()
				});		
			}
		});
		
	});
	//end get list of winners.

	//get config data.
	app.get('/api/config', function(req, res){
		Properties.find(function(err, returnProperties){
			if(err){
				res.json(500, { success : false, message: err });
			}else if(returnProperties && returnProperties.length > 0){
				var returnPropertiesObject = returnProperties[0];
				res.render('edit_config.html', {
					config: returnPropertiesObject
				});		
			}else{
				res.json(200, {success : false, message: "Properties not found."});
			}
		});
		
	});
	//end get config data.

	//update edit config info
	app.post('/api/editconfig', function(req, res){
		var repeattime = req.body.repeattime,
			relaxtime = req.body.relaxtime,
			distance = req.body.distance;
		if(!repeattime || !relaxtime || !distance){
			res.send(500,'showAlert') 
		}else{
			Properties.find(function(err, returnProperties){
				if(err){
					res.json(500, { success : false, message: err });
				}else if(returnProperties && returnProperties.length > 0){
					var returnPropertiesObject = returnProperties[0];
					returnPropertiesObject.repeattime = repeattime;
					returnPropertiesObject.relaxtime = relaxtime;
					returnPropertiesObject.distance = distance;
					
					returnPropertiesObject.save(function(err) {
						if(err){
							res.json(500,{success : false, message : err});
						}

						res.end('{"success" : "Updated Successfully", "status" : 200}');
					});
				}else{
					res.json(200, {success : false, message: "Properties not found."});
				}
			});

		}	
	});
	//end update edit config info
	
	//get user history.
	app.get('/api/viewhistory/:userId', function(req, res){
		var userId = req.params.userId;
		User.find({userId : userId} ,function(err, returnUsers){
			if(err){
				res.json(500, { success : false, message: err });
			}else if(returnUsers && returnUsers.length > 0){
				res.render('user_history.html', {
					username : returnUsers.username,
					beacons : returnUsers[0].beacon.sort(function(a,b){return new Date(a.date) - new Date(b.date);}),
					displayTime: new Date()			
				});		
			}
		});
		
	});
	//end get user history.

	//get user tracking page.
	app.get('/usertracking', function(req, res){
		var arrayBeaconFoundCount = [];
		// User.find({} ,function(err, returnUsers){
		// 	if(err){
		// 		res.json(500, { success : false, message: err });
		// 	}else if(returnUsers && returnUsers.length > 0){
		// 		for (var i = 0; i < returnUsers.length; i++) {
		// 			var countBeaconFound = 0;
		// 			for (var x = 0; x < returnUsers[i].beacon.length; x++) {
						
		// 				if(returnUsers[i].beacon[x].detected){
		// 					countBeaconFound++;
		// 					// console.log(returnUsers[i].beacon[x].detected + '/' + returnUsers[i].username);
		// 					// console.log(returnUsers.length + '/' + returnUsers[i].beacon.length);	
		// 				}
						
		// 			}
		// 			arrayBeaconFoundCount.push(countBeaconFound);
		// 		}
		// 		// arrayBeaconFoundCount.sort(function(a, b){return b-a});
		// 		// console.log(arrayBeaconFoundCount +'/'+arrayBeaconFoundCount.length);
		// 		res.render('user_tracking.html', {
		// 			users : returnUsers,	
		// 			beaconFound : arrayBeaconFoundCount
		// 		});		
		// 	}else{
		// 		res.render('user_tracking.html', {
		// 			users : returnUsers,	
		// 			beaconFound : arrayBeaconFoundCount
		// 		});
		// 	}	
		// });
		User.find({} ,function(err, returnUsers){
			if(err){
				res.json(500, { success : false, message: err });
			}else if(returnUsers && returnUsers.length > 0){
				res.render('user_tracking.html', {
					users : returnUsers
				});	
			}else{
				res.render('user_tracking.html', {
					users : returnUsers
				});
			}
		}).sort({beaconFound : -1});	
	});
	//end get user tracking page.


};