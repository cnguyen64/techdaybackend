var mongoose = require('mongoose'),
	beaconSchema = require('../models/beacon').beaconSchema;
	propertiesSchema = require('../models/properties').propertiesSchema;
  	Schema = mongoose.Schema;

var userSchema = new Schema({
    username        : { type: String },
    userId 			: { type: String },
    email 			: { type: String },
    beaconFound : {type : Number},
    beacon  : [beaconSchema],
    properties : [propertiesSchema]
	});
// disable autoIndex since index creation can cause a significant performance impact.
userSchema.set('autoIndex', false);
// define index
userSchema.index({ username: 1 });
// create model
var user = mongoose.model('user', userSchema);

module.exports = {
  User: user
};