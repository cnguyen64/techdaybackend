var mongoose = require('mongoose'),
  	Schema = mongoose.Schema;

var clueSchema = new Schema({
    clue        : { type: String },  
    idnumber          :{type: Number}
 

});
// disable autoIndex since index creation can cause a significant performance impact.
clueSchema.set('autoIndex', false);
// define index
clueSchema.index({ idnumber: 1 });


// create model
var clue = mongoose.model('clue', clueSchema);

module.exports = {
  Clue: clue,
  clueSchema : clueSchema
};