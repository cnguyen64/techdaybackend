var mongoose = require('mongoose'),
  	Schema = mongoose.Schema;

var winnerSchema = new Schema({
    username        : { type: String },
    userId 			: {type: String},
    date 			: {type: String} 
	});
// disable autoIndex since index creation can cause a significant performance impact.
winnerSchema.set('autoIndex', false);
// define index
winnerSchema.index({ username: 1 });
// create model
var winner = mongoose.model('winner', winnerSchema);

module.exports = {
  Winner: winner
};