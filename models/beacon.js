var mongoose = require('mongoose'),
  	Schema = mongoose.Schema;

var beaconSchema = new Schema({
    beaconId        : { type: String },  
    major        : { type: String },
    minor  		: { type: String },
    date      : {type: String},
    name 		: {type: String},
    dependency :{type: String}, 
    clue         : {type: String},
    idnumber          :{type: Number},
    detected		: {type : Boolean},
    detectingcount : { type : Array , "default" : [] }
 

});
// disable autoIndex since index creation can cause a significant performance impact.
beaconSchema.set('autoIndex', false);
// define index
beaconSchema.index({ beaconId: 1 });


// create model
var beacon = mongoose.model('beaconId', beaconSchema);

module.exports = {
  Beacon: beacon,
  beaconSchema : beaconSchema
};