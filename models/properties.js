var mongoose = require('mongoose'),
  	Schema = mongoose.Schema;

var propertiesSchema = new Schema({
    repeattime        : { type: Number },  
    relaxtime       : { type: String },
    distance  		: { type: Number }
});
// disable autoIndex since index creation can cause a significant performance impact.
propertiesSchema.set('autoIndex', false);
// define index
propertiesSchema.index({ repeattime: 1 });


// create model
var properties = mongoose.model('properties', propertiesSchema);

module.exports = {
  Properties: properties,
  propertiesSchema : propertiesSchema
};