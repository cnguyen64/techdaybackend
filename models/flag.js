var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var flagSchema = new Schema({
    flagID: {type: String},
    isOn  : Boolean
  });
// disable autoIndex since index creation can cause a significant performance impact.
flagSchema.set('autoIndex', false);
// define index
flagSchema.index({ flagID : 1 });
// create model
var flag = mongoose.model('flag', flagSchema);

module.exports = {
  Flag: flag
};

