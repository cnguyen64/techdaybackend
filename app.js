/*jshint node:true*/

// app.js
// This file contains the server side JavaScript code for your application.
// This sample application uses express as web application framework (http://expressjs.com/),
// and jade as template engine (http://jade-lang.com/).

var express = require('express'),
 	errorHandler = require('errorhandler'),
    bodyParser = require('body-parser'),
    swig = require('swig'),
    http = require('http'),
    fs = require('fs-extra'),
    mongoose = require('mongoose');
    //connect db
// var mongoUri = "mongodb://127.0.0.1:27017/tech_up";
// var mongoUri = "mongodb://asset.catalog:    @ds047911.mongolab.com:47911/tech_com";
var mongoUri = "mongodb://techupadmin1:password123@c740.candidate.49.mongolayer.com:10740/techup";

    mongoose.connect(mongoUri, function (err, res) {
	    if (err) {
	      console.log ('ERROR connect to MongoDB : ' + err);
	    } else {
	      console.log ('CONNECTED to MongoDB');
	    }
  	});
    //CORS middleware
var allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');

    next();
}

// setup middleware
var app = express();    
    app.use(bodyParser());
    app.use(errorHandler());    
    app.use(allowCrossDomain);
    app.use(express.static(__dirname + '/public')); //setup static public directory 
    app.set('views', __dirname + '/views'); //optional since express defaults to CWD/views
    app.set('view engine', 'html');
    app.set('view options', { layout: false });
    app.engine('html', swig.renderFile);


require('./routes/api')(app);
// render index page
app.get('/', function(req, res){
//	res.render('index.html');
	res.redirect('/api/listbeacon');
});
app.post('/receive', function(req, res) {
    var body = '';
    filePath = __dirname + '/public/data.plist';
    req.on('data', function(data) {
        body += data;
    });

    req.on('end', function (){
        fs.appendFile(filePath, body, function() {
            res.end();
        });
    });
});
app.get('/resetdata', function(req, res){
	res.render('reset_data.html');
});

// There are many useful environment variables available in process.env.
// VCAP_APPLICATION contains useful information about a deployed application.
var appInfo = JSON.parse(process.env.VCAP_APPLICATION || "{}");

// TODO: Get application information and use it in your app.

// VCAP_SERVICES contains all the credentials of services bound to
// this application. For details of its content, please refer to
// the document or sample of each service.
var services = JSON.parse(process.env.VCAP_SERVICES || "{}");
// TODO: Get service credentials and communicate with bluemix services.
// The IP address of the Cloud Foundry DEA (Droplet Execution Agent) that hosts this application:
var host = (process.env.VCAP_APP_HOST || 'localhost');
// The port on the DEA for communication with the application:
var port = (process.env.VCAP_APP_PORT || 3000);
// Start server
app.listen(port, host);

//app.listen(80, '172.16.0.27');

console.log('App started on port ' + port);

